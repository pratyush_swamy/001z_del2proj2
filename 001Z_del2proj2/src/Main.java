/*****************************
 * Team 001Z
 * Team Memebers - ggsingh, hsure, pswamy
 * Project 2 - Deliverable 2
 * CSC541
 * Date - 11/18/2013

 ******************************/
 import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Main extends DefaultHandler
{
    private static final String jdbcURL = "jdbc:oracle:thin:@ora.csc.ncsu.edu:1521:orcl";
    static Connection connection = null ;

    private static final String user = "ggsingh";
    private static final String password = "541001Z";

    private static final String[] tableNames = { "servicendpoint",
            "device_sets",
            "domain_deployPolicy",
            "domain",
            "deploymentPolicy",
            "managed_sets",
            "device"};

    private static final String truncateQueryString = "DELETE FROM ";
    static PreparedStatement truncateQuery;

    static String insertDevice = "INSERT into DEVICE " +
            "(id, type, guiPort, hlmPort, currentAmpVersion, quiesceTimeout, featureLicenses) VALUES " +
            "(?, ?, ?, ?, ?, ?, ?)";
    static PreparedStatement insertDeviceQuery;

    static String insertDomain = "INSERT into DOMAIN " +
            "(id, highestVersion, synchDate, outOfSynch, quiesceTimeout, synchMode, deviceId) VALUES " +
            "(?, ?, ?, ?, ?, ?, ?)";
    static PreparedStatement insertDomainQuery;

    static String insertDeploymentPolicy = "INSERT into DEPLOYMENTPOLICY " +
            "(id, highestVersion, synchDate, policyType) VALUES " +
            "(?, ?, ?, ?)";
    static PreparedStatement insertDeployPolicyQuery;

    static String insertMngdSet = "INSERT into MANAGED_SETS " +
            "(id) VALUES " +
            "(?)";
    static PreparedStatement insertMngdSetQuery;

    static Map<String,ArrayList<String>> deviceSets;

    static String insertDomainDeployPolicy = "INSERT into DOMAIN_DEPLOYPOLICY " +
            "(domain_id, policy_id) VALUES " +
            "(?, ?)";
    static PreparedStatement insertDomainDeployPolicyQuery;

    static String insertServiceEndpoint = "INSERT into SERVICENDPOINT " +
            "(id, type, operation, port, targetServer, policy_id) VALUES " +
            "(?, ?, ?, ?, ?, ?)";
    static PreparedStatement insertServiceEndpointQuery;

    ArrayList<String> ele_ids = new ArrayList<String>();

    private static String fileName;

    public void initializeDB()
    {
        try
        {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(jdbcURL, user, password);
            connection.setAutoCommit(false);

            insertDeviceQuery = connection.prepareStatement(insertDevice);
            insertDomainQuery = connection.prepareStatement(insertDomain);
            insertDeployPolicyQuery = connection.prepareStatement(insertDeploymentPolicy);
            insertDomainDeployPolicyQuery = connection.prepareStatement(insertDomainDeployPolicy);
            insertServiceEndpointQuery = connection.prepareStatement(insertServiceEndpoint);
            insertMngdSetQuery = connection.prepareStatement(insertMngdSet);


            //Delete all rows from all tables.
            for(int i=0;i<tableNames.length; i++)
            {
                truncateQuery = connection.prepareStatement(truncateQueryString + tableNames[i]);
                truncateQuery.executeUpdate();
                truncateQuery.clearParameters();
                truncateQuery.close();
            }
            //System.out.println("Deleted Contents from Tables!!");
            connection.commit();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println("Error in loading the Database.");
            System.exit(0);
        }
        finally
        {
            try
            {
                truncateQuery.close();
            }
            catch(Exception e)
            {
                System.out.println("Error in closing statements");
            }

        }
    }

    public void runBatchQueries(){
        try
        {
            int[] count1 = insertDeviceQuery.executeBatch();
            connection.commit();

            int[] count2 = insertDomainQuery.executeBatch();
            connection.commit();

            int[] count3 = insertDeployPolicyQuery.executeBatch();
            connection.commit();

            int[] count4 = insertDomainDeployPolicyQuery.executeBatch();
            connection.commit();

            int[] count5 = insertServiceEndpointQuery.executeBatch();
            connection.commit();

            int[] count6 = insertMngdSetQuery.executeBatch();
            connection.commit();

            insertServiceEndpointQuery.close();
            insertDomainQuery.close();
            insertDeployPolicyQuery.close();
            insertDomainDeployPolicyQuery.close();
            insertServiceEndpointQuery.close();
            insertMngdSetQuery.close();

            // Adding device_sets queries
            // Device and set pairs
            String insertDeviceSet = "INSERT into DEVICE_SETS " +
                    "(set_id, device_id) VALUES " +
                    "(?, ?)";
            PreparedStatement insertDeviceSetQuery = null;

            try
            {
                for(String setID : deviceSets.keySet())
                {

                    //insertDeviceSetQuery.setString(1, setID);
                    for(String deviceID : deviceSets.get(setID))
                    {
                        insertDeviceSetQuery = connection.prepareStatement(insertDeviceSet);
                        insertDeviceSetQuery.setString(1, setID);
                        insertDeviceSetQuery.setString(2, deviceID);
                        insertDeviceSetQuery.executeUpdate();
                        insertDeviceSetQuery.clearParameters();
                        insertDeviceSetQuery.close();
                    }
                }
            }
            catch(Exception e)
            {
                if(!e.getMessage().contains("unique constraint (GGSINGH.SYS_C00379977) violated"))
                {
                    e.printStackTrace();
                    System.out.println("Error during database creation/database update");
                }
            }
            finally
            {
                try
                {
                    if(insertDeviceSetQuery!=null)
                    {
                        //insertDeviceSetQuery.close();
                    }
                }
                catch(Exception e)
                {
                    System.out.println("Error in closing statements");
                }

            }
            // closing device_sets queries

        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println("Database Connection issues.");
        }
    }


    public static void main(String[] args)
    {
        Statement stmt = null;
        ResultSet result = null;
        int count = 0;
        try
        {
            Main main = new Main();

            if(args.length < 1)
            {
                System.out.println("Enter the name of the XML file");
                System.exit(0);
            }

            fileName = args[0];

            // Load the connection and,
            // Delete existing rows from all tables.
            main.initializeDB();

            deviceSets = new HashMap<String, ArrayList<String>>();

            // Perform all the insertions during the configure operation.
            main.configureXML(fileName);

            main.runBatchQueries();

            // Basic JDBC count queries for the number of rows in the devices and deploymentPolicy tables.
            stmt = connection.createStatement();
            result = stmt.executeQuery("select count(id) from device");
            while (result.next())
            {
                count = result.getInt(1);
            }

            System.out.print(count);

            stmt.close();
            result.close();

            stmt = connection.createStatement();
            result = stmt.executeQuery("select count(id) from deploymentPolicy");
            while (result.next())
            {
                count = result.getInt(1);
            }
            System.out.println(" "+count);

            stmt.close();
            connection.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.println("Database Connection issues.");
        }
        finally
        {
            try
            {
                stmt.close();
                result.close();
                connection.close();
            }
            catch(Exception e)
            {
                System.out.println("Error in closing statements");
            }

        }
    }

    //

    // Stupid Hack for the deviceId 
    public String transformToDeviceId(String device)
    {
        return "DPD"+device.substring(1);
    }

    // Remove these if not needed.
    private String device_id;
    private String domain_id;
    private String deployment_id;
    private String servicepoint_id;
    private String manageset_id;
    private boolean currentDevice = false;

    public void startElement(String URI, String localString,
                             String qName, Attributes attr) throws SAXException
    {
        try
        {
            if (qName.equalsIgnoreCase("devices"))
            {
                device_id = attr.getValue("xmi:id");
                if(!ele_ids.contains(device_id)){
                    ele_ids.add(device_id);
                    insertDeviceQuery.setString(1, attr.getValue("xmi:id"));
                    insertDeviceQuery.setString(2, attr.getValue("deviceType"));
                    insertDeviceQuery.setString(3, attr.getValue("GUIPort"));
                    insertDeviceQuery.setString(4, attr.getValue("HLMPort"));
                    insertDeviceQuery.setString(5, attr.getValue("currentAMPVersion"));
                    insertDeviceQuery.setString(6, attr.getValue("quiesceTimeout"));
                    insertDeviceQuery.setString(7, attr.getValue("featureLicenses"));
                    insertDeviceQuery.addBatch();
                }
                //System.out.println("Inserting device - " +device_id);
            }
            if(qName.equalsIgnoreCase("domains"))
            {
                domain_id = attr.getValue("xmi:id");
                if(!ele_ids.contains(domain_id)){
                    ele_ids.add(domain_id);
                    insertDomainQuery.setString(1, attr.getValue("xmi:id"));
                    insertDomainQuery.setString(2, attr.getValue("highestVersion"));
                    insertDomainQuery.setString(3, attr.getValue("synchDate"));
                    insertDomainQuery.setString(4, attr.getValue("OutOfSynch"));
                    insertDomainQuery.setString(5, attr.getValue("quiesceTimeout"));
                    insertDomainQuery.setString(6, attr.getValue("synchDate"));
                    insertDomainQuery.setString(7, device_id);
                    insertDomainQuery.addBatch();
                }
                //System.out.println("Inserting Domain - " + domain_id);
            }
            if(qName.equalsIgnoreCase("deploymentPolicy"))
            {
                deployment_id = attr.getValue("xmi:id");
                if(!ele_ids.contains(deployment_id)){
                    ele_ids.add(deployment_id);
                    insertDeployPolicyQuery.setString(1, attr.getValue("xmi:id"));
                    insertDeployPolicyQuery.setString(2, attr.getValue("highestVersion"));
                    insertDeployPolicyQuery.setString(3, attr.getValue("synchDate"));
                    insertDeployPolicyQuery.setString(4, attr.getValue("policyType"));
                    insertDeployPolicyQuery.addBatch();
                    //System.out.println(domain_id+"\t"+deployment_id);
                    insertDomainDeployPolicyQuery.setString(1, domain_id);
                    insertDomainDeployPolicyQuery.setString(2, deployment_id);
                    insertDomainDeployPolicyQuery.addBatch();
                }
                //System.out.println("Inserting Deployment Policy - " + deployment_id);
            }
            if(qName.equalsIgnoreCase("serviceend-point"))
            {
                servicepoint_id = attr.getValue("xmi:id");
                if(!ele_ids.contains(servicepoint_id)){
                    ele_ids.add(servicepoint_id);
                    insertServiceEndpointQuery.setString(1, attr.getValue("xmi:id"));
                    insertServiceEndpointQuery.setString(2, attr.getValue("type"));
                    insertServiceEndpointQuery.setString(3, attr.getValue("operation"));
                    insertServiceEndpointQuery.setString(4, attr.getValue("port"));
                    insertServiceEndpointQuery.setString(5, attr.getValue("targetserver"));
                    insertServiceEndpointQuery.setString(6, deployment_id);
                    insertServiceEndpointQuery.addBatch();
                }
                //System.out.println("Inserting Service End Points - " + attr.getValue("xmi:id"));
            }
            if(qName.equalsIgnoreCase("managedSets"))
            {
                manageset_id = attr.getValue("xmi:id");
                if(!ele_ids.contains(manageset_id)){
                    ele_ids.add(manageset_id);
                    insertMngdSetQuery.setString(1, attr.getValue("xmi:id"));
                    insertMngdSetQuery.addBatch();

                    String currentMngdSet = attr.getValue("xmi:id");
                    //System.out.println("Inserting Managed Sets - " + currentMngdSet);

                    if(attr.getValue("devicemembers") != null && !attr.getValue("devicemembers").isEmpty()){
                        String[] deviceList = attr.getValue("devicemembers").split(",");
                        ArrayList<String> temp;
                        for(int i = 0; i<deviceList.length;i++)
                        {
                            if(!deviceSets.containsKey(currentMngdSet))
                            {
                                temp = new ArrayList<String>();
                            }
                            else
                            {
                                temp = deviceSets.get(currentMngdSet);
                            }
                            temp.add(transformToDeviceId(deviceList[i]));
                            deviceSets.put(currentMngdSet, temp);
                        }
                    }
                }
            }
        }
        catch(Exception e)
        {
            if(!e.getMessage().contains("unique constraint"))
            {
                e.printStackTrace();
                System.out.println("Error during database creation/database update");
            }
        }
    }

    public void endElement(String URI, String localString,
                           String qName) throws SAXException
    {

    }


    public void configureXML(String XMLFile)
    {
        try
        {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(XMLFile, new Main());

        }
        catch (Exception e)
        {
            System.out.println("Error in loading file.");
        }
    }
}